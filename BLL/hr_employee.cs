﻿/*
* hr_employee.cs
*
* 功 能： N/A
* 类 名： hr_employee
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-24 11:20:12    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Collections.Generic;
using System.Data;
using XHD.Common;

namespace XHD.BLL
{
    /// <summary>
    ///     hr_employee
    /// </summary>
    public class hr_employee
    {
        private readonly DAL.hr_employee dal = new DAL.hr_employee();

        #region  BasicMethod

        /// <summary>
        ///     增加一条数据
        /// </summary>
        public int Add(Model.hr_employee model)
        {
            return dal.Add(model);
        }

        /// <summary>
        ///     更新一条数据
        /// </summary>
        public bool Update(Model.hr_employee model)
        {
            return dal.Update(model);
        }

        /// <summary>
        ///     删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {
            return dal.Delete(ID);
        }

        /// <summary>
        ///     删除一条数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            return dal.DeleteList(PageValidate.SafeLongFilter(IDlist, 0));
        }

        /// <summary>
        ///     得到一个对象实体
        /// </summary>
        public Model.hr_employee GetModel(int ID)
        {
            return dal.GetModel(ID);
        }

        /// <summary>
        ///     得到一个对象实体，从缓存中
        /// </summary>
        public Model.hr_employee GetModelByCache(int ID)
        {
            string CacheKey = "hr_employeeModel-" + ID;
            object objModel = DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(ID);
                    if (objModel != null)
                    {
                        int ModelCache = ConfigHelper.GetConfigInt("ModelCache");
                        DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch
                {
                }
            }
            return (Model.hr_employee) objModel;
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }

        /// <summary>
        ///     获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public List<Model.hr_employee> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public List<Model.hr_employee> DataTableToList(DataTable dt)
        {
            var modelList = new List<Model.hr_employee>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                Model.hr_employee model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = dal.DataRowToModel(dt.Rows[n]);
                    if (model != null)
                    {
                        modelList.Add(model);
                    }
                }
            }
            return modelList;
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        ///     分页获取数据列表
        ///     分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
        {
            return dal.GetList(PageSize, PageIndex, strWhere, filedOrder, out Total);
        }

        #endregion  BasicMethod

        #region  ExtensionMethod

        /// <summary>
        ///     更新岗位
        /// </summary>
        public bool UpdatePost(Model.hr_employee model)
        {
            return dal.UpdatePost(model);
        }
        public bool UpdateDefaultCity(Model.hr_employee model)
        {
            return dal.UpdateDefaultCity(model);
        }
        /// <summary>
        ///     获取密码
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public DataSet GetPWD(int ID)
        {
            return dal.GetPWD(ID);
        }

        /// <summary>
        ///     更新密码
        /// </summary>
        public bool changepwd(Model.hr_employee model)
        {
            return dal.changepwd(model);
        }

        /// <summary>
        ///     获取权限
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public DataSet GetRole(int ID)
        {
            return dal.GetRole(ID);
        }

        /// <summary>
        ///     个人信息修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool PersonalUpdate(Model.hr_employee model)
        {
            return dal.PersonalUpdate(model);
        }

        #endregion  ExtensionMethod
    }
}