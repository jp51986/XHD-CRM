﻿/*
* Sys_log.cs
*
* 功 能： N/A
* 类 名： Sys_log
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-24 18:54:40    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using XHD.DBUtility;

//Please add references

namespace XHD.DAL
{
    /// <summary>
    ///     数据访问类:Sys_log
    /// </summary>
    public class Sys_log
    {
        #region  BasicMethod	

        /// <summary>
        ///     增加一条数据
        /// </summary>
        public int Add(Model.Sys_log model)
        {
            var strSql = new StringBuilder();
            strSql.Append("insert into Sys_log(");
            strSql.Append("EventType,EventID,EventTitle,UserID,IPStreet,EventDate,Log_Content)");
            strSql.Append(" values (");
            strSql.Append("@EventType,@EventID,@EventTitle,@UserID,@IPStreet,@EventDate,@Log_Content)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters =
            {
                new SqlParameter("@EventType", SqlDbType.VarChar, 250),
                new SqlParameter("@EventID", SqlDbType.VarChar, 50),
                new SqlParameter("@EventTitle", SqlDbType.VarChar, 250),
                new SqlParameter("@UserID", SqlDbType.Int, 4),
                new SqlParameter("@IPStreet", SqlDbType.VarChar, 50),
                new SqlParameter("@EventDate", SqlDbType.DateTime),
                new SqlParameter("@Log_Content", SqlDbType.NVarChar, -1)
            };
            parameters[0].Value = model.EventType;
            parameters[1].Value = model.EventID;
            parameters[2].Value = model.EventTitle;
            parameters[3].Value = model.UserID;
            parameters[4].Value = model.IPStreet;
            parameters[5].Value = model.EventDate;
            parameters[6].Value = model.Log_Content;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            return Convert.ToInt32(obj);
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            var strSql = new StringBuilder();
            strSql.Append("select id,EventType,EventID,EventTitle,UserID,IPStreet,EventDate,Log_Content ");
            strSql.Append(",(select name from hr_employee where id = Sys_log.[UserID]) as [UserName] ");
            strSql.Append(" FROM Sys_log ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        ///     获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            var strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top);
            }
            strSql.Append(" id,EventType,EventID,EventTitle,UserID,IPStreet,EventDate,Log_Content ");
            strSql.Append(",(select name from hr_employee where id = Sys_log.[UserID]) as [UserName] ");
            strSql.Append(" FROM Sys_log ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        ///     分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
        {
            var strSql_grid = new StringBuilder();
            var strSql_total = new StringBuilder();
            strSql_total.Append(" SELECT COUNT(id) FROM Sys_log ");
            strSql_grid.Append("SELECT ");
            strSql_grid.Append("      id,EventType,EventID,EventTitle,UserID,IPStreet,EventDate,Log_Content ");
            strSql_grid.Append(",(select name from hr_employee where id = w1.[UserID]) as [UserName] ");
            strSql_grid.Append(
                " FROM ( SELECT id,EventType,EventID,EventTitle,UserID,IPStreet,EventDate,Log_Content, ROW_NUMBER() OVER( Order by " +
                filedOrder + " ) AS n from Sys_log");
            if (strWhere.Trim() != "")
            {
                strSql_grid.Append(" WHERE " + strWhere);
                strSql_total.Append(" WHERE " + strWhere);
            }
            strSql_grid.Append("  ) as w1  ");
            strSql_grid.Append("WHERE n BETWEEN " + PageSize*(PageIndex - 1) + " AND " + PageSize*PageIndex);
            strSql_grid.Append(" ORDER BY " + filedOrder);
            Total = DbHelperSQL.Query(strSql_total.ToString()).Tables[0].Rows[0][0].ToString();
            return DbHelperSQL.Query(strSql_grid.ToString());
        }

        #endregion  BasicMethod

        #region  ExtensionMethod

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetLogtype()
        {
            var strSql = new StringBuilder();
            strSql.Append("select distinct EventType FROM Sys_log order by EventType");

            return DbHelperSQL.Query(strSql.ToString());
        }

        #endregion  ExtensionMethod
    }
}