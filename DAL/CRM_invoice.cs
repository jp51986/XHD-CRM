﻿/*
* CRM_invoice.cs
*
* 功 能： N/A
* 类 名： CRM_invoice
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-24 09:38:26    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using XHD.DBUtility;

//Please add references

namespace XHD.DAL
{
    /// <summary>
    ///     数据访问类:CRM_invoice
    /// </summary>
    public class CRM_invoice
    {
        #region  BasicMethod

        /// <summary>
        ///     增加一条数据
        /// </summary>
        public int Add(Model.CRM_invoice model)
        {
            var strSql = new StringBuilder();
            strSql.Append("insert into CRM_invoice(");
            strSql.Append("Customer_id,invoice_num,invoice_type_id,invoice_amount,invoice_content,invoice_date,C_depid,C_empid,create_id,create_date,order_id,isDelete,Delete_time)");
            strSql.Append(" values (");
            strSql.Append( "@Customer_id,@invoice_num,@invoice_type_id,@invoice_amount,@invoice_content,@invoice_date,@C_depid,@C_empid,@create_id,@create_date,@order_id,@isDelete,@Delete_time)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters =
            {
                new SqlParameter("@Customer_id", SqlDbType.Int, 4),
                new SqlParameter("@invoice_num", SqlDbType.VarChar, 250),
                new SqlParameter("@invoice_type_id", SqlDbType.Int, 4),
                new SqlParameter("@invoice_amount", SqlDbType.Float, 8),
                new SqlParameter("@invoice_content", SqlDbType.VarChar, -1),
                new SqlParameter("@invoice_date", SqlDbType.DateTime),
                new SqlParameter("@C_depid", SqlDbType.Int, 4),
                new SqlParameter("@C_empid", SqlDbType.Int, 4),
                new SqlParameter("@create_id", SqlDbType.Int, 4),
                new SqlParameter("@create_date", SqlDbType.DateTime),
                new SqlParameter("@order_id", SqlDbType.Int, 4),
                new SqlParameter("@isDelete", SqlDbType.Int, 4),
                new SqlParameter("@Delete_time", SqlDbType.DateTime)
            };
            parameters[0].Value = model.Customer_id;
            parameters[1].Value = model.invoice_num;
            parameters[2].Value = model.invoice_type_id;
            parameters[3].Value = model.invoice_amount;
            parameters[4].Value = model.invoice_content;
            parameters[5].Value = model.invoice_date;
            parameters[6].Value = model.C_depid;
            parameters[7].Value = model.C_empid;
            parameters[8].Value = model.create_id;
            parameters[9].Value = model.create_date;
            parameters[10].Value = model.order_id;
            parameters[11].Value = model.isDelete;
            parameters[12].Value = model.Delete_time;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            return Convert.ToInt32(obj);
        }

        /// <summary>
        ///     更新一条数据
        /// </summary>
        public bool Update(Model.CRM_invoice model)
        {
            var strSql = new StringBuilder();
            strSql.Append("update CRM_invoice set ");
            strSql.Append("Customer_id=@Customer_id,");
            strSql.Append("invoice_num=@invoice_num,");
            strSql.Append("invoice_type_id=@invoice_type_id,");
            strSql.Append("invoice_amount=@invoice_amount,");
            strSql.Append("invoice_content=@invoice_content,");
            strSql.Append("invoice_date=@invoice_date,");
            strSql.Append("C_depid=@C_depid,");
            strSql.Append("C_empid=@C_empid");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters =
            {
                new SqlParameter("@Customer_id", SqlDbType.Int, 4),
                new SqlParameter("@invoice_num", SqlDbType.VarChar, 250),
                new SqlParameter("@invoice_type_id", SqlDbType.Int, 4),
                new SqlParameter("@invoice_amount", SqlDbType.Float, 8),
                new SqlParameter("@invoice_content", SqlDbType.VarChar, -1),
                new SqlParameter("@invoice_date", SqlDbType.DateTime),
                new SqlParameter("@C_depid", SqlDbType.Int, 4),
                new SqlParameter("@C_empid", SqlDbType.Int, 4),
                new SqlParameter("@id", SqlDbType.Int, 4)
            };
            parameters[0].Value = model.Customer_id;
            parameters[1].Value = model.invoice_num;
            parameters[2].Value = model.invoice_type_id;
            parameters[3].Value = model.invoice_amount;
            parameters[4].Value = model.invoice_content;
            parameters[5].Value = model.invoice_date;
            parameters[6].Value = model.C_depid;
            parameters[7].Value = model.C_empid;
            parameters[8].Value = model.id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     删除一条数据
        /// </summary>
        public bool Delete(int id)
        {
            var strSql = new StringBuilder();
            strSql.Append("delete from CRM_invoice ");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters =
            {
                new SqlParameter("@id", SqlDbType.Int, 4)
            };
            parameters[0].Value = id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     批量删除数据
        /// </summary>
        public bool DeleteList(string idlist)
        {
            var strSql = new StringBuilder();
            strSql.Append("delete from CRM_invoice ");
            strSql.Append(" where id in (" + idlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            return false;
        }


        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            var strSql = new StringBuilder();
            strSql.Append(
                "select id,Customer_id,invoice_num,invoice_type_id,invoice_amount,invoice_content,invoice_date,C_depid,C_empid,create_id,create_date,order_id,isDelete,Delete_time ");
            strSql.Append(
                " ,(select Customer from CRM_Customer where id = CRM_invoice.[Customer_id]) as [Customer_name]  ");
            strSql.Append(
                " ,(select params_name from Param_SysParam where id = CRM_invoice.[invoice_type_id]) as [invoice_type]  ");
            strSql.Append(" ,(select d_name from hr_department where id = CRM_invoice.[C_depid]) as [C_depname]   ");
            strSql.Append(" ,(select name from hr_employee where id = CRM_invoice.[C_empid]) as [C_empname]  ");
            strSql.Append(" ,(select name from hr_employee where id = CRM_invoice.[create_id]) as [create_name]  ");
            strSql.Append(" FROM CRM_invoice ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        ///     获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            var strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top);
            }
            strSql.Append(
                " id,Customer_id,invoice_num,invoice_type_id,invoice_amount,invoice_content,invoice_date,C_depid,C_empid,create_id,create_date,order_id,isDelete,Delete_time ");
            strSql.Append(
                " ,(select Customer from CRM_Customer where id = CRM_invoice.[Customer_id]) as [Customer_name]  ");
            strSql.Append(
                " ,(select params_name from Param_SysParam where id = CRM_invoice.[invoice_type_id]) as [invoice_type]  ");
            strSql.Append(" ,(select d_name from hr_department where id = CRM_invoice.[C_depid]) as [C_depname]   ");
            strSql.Append(" ,(select name from hr_employee where id = CRM_invoice.[C_empid]) as [C_empname]  ");
            strSql.Append(" ,(select name from hr_employee where id = CRM_invoice.[create_id]) as [create_name]  ");
            strSql.Append(" FROM CRM_invoice ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        ///     分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
        {
            var strSql_grid = new StringBuilder();
            var strSql_total = new StringBuilder();
            strSql_total.Append(" SELECT COUNT(id) FROM CRM_invoice ");
            strSql_grid.Append("SELECT ");
            strSql_grid.Append(
                "      id,Customer_id,invoice_num,invoice_type_id,invoice_amount,invoice_content,invoice_date,C_depid,C_empid,create_id,create_date,order_id,isDelete,Delete_time ");
            strSql_grid.Append(" ,(select Customer from CRM_Customer where id = w1.[Customer_id]) as [Customer_name]  ");
            strSql_grid.Append(
                " ,(select params_name from Param_SysParam where id = w1.[invoice_type_id]) as [invoice_type]  ");
            strSql_grid.Append(" ,(select d_name from hr_department where id = w1.[C_depid]) as [C_depname]   ");
            strSql_grid.Append(" ,(select name from hr_employee where id = w1.[C_empid]) as [C_empname]  ");
            strSql_grid.Append(" ,(select name from hr_employee where id = w1.[create_id]) as [create_name]  ");
            strSql_grid.Append(
                " FROM ( SELECT id,Customer_id,invoice_num,invoice_type_id,invoice_amount,invoice_content,invoice_date,C_depid,C_empid,create_id,create_date,order_id,isDelete,Delete_time, ROW_NUMBER() OVER( Order by " +
                filedOrder + " ) AS n from CRM_invoice");
            if (strWhere.Trim() != "")
            {
                strSql_grid.Append(" WHERE " + strWhere);
                strSql_total.Append(" WHERE " + strWhere);
            }
            strSql_grid.Append("  ) as w1  ");
            strSql_grid.Append("WHERE n BETWEEN " + PageSize*(PageIndex - 1) + " AND " + PageSize*PageIndex);
            strSql_grid.Append(" ORDER BY " + filedOrder);
            Total = DbHelperSQL.Query(strSql_total.ToString()).Tables[0].Rows[0][0].ToString();
            return DbHelperSQL.Query(strSql_grid.ToString());
        }

        #endregion  BasicMethod

        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}