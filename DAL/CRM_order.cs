﻿/*
* CRM_order.cs
*
* 功 能： N/A
* 类 名： CRM_order
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 19:49:34    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using XHD.DBUtility;

//Please add references

namespace XHD.DAL
{
    /// <summary>
    ///     数据访问类:CRM_order
    /// </summary>
    public class CRM_order
    {
        #region  BasicMethod	

        /// <summary>
        ///     增加一条数据
        /// </summary>
        public int Add(Model.CRM_order model)
        {
            var strSql = new StringBuilder();
            strSql.Append("insert into CRM_order(");
            strSql.Append(
                "Serialnumber,Customer_id,Order_date,pay_type_id,Order_details,Order_status_id,Order_amount,create_id,create_date,C_dep_id,C_emp_id,F_dep_id,F_emp_id,receive_money,arrears_money,invoice_money,arrears_invoice,isDelete,Delete_time)");
            strSql.Append(" values (");
            strSql.Append(
                "@Serialnumber,@Customer_id,@Order_date,@pay_type_id,@Order_details,@Order_status_id,@Order_amount,@create_id,@create_date,@C_dep_id,@C_emp_id,@F_dep_id,@F_emp_id,@receive_money,@arrears_money,@invoice_money,@arrears_invoice,@isDelete,@Delete_time)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters =
            {
                new SqlParameter("@Serialnumber", SqlDbType.VarChar, 250),
                new SqlParameter("@Customer_id", SqlDbType.Int, 4),
                new SqlParameter("@Order_date", SqlDbType.DateTime),
                new SqlParameter("@pay_type_id", SqlDbType.Int, 4),
                new SqlParameter("@Order_details", SqlDbType.VarChar, -1),
                new SqlParameter("@Order_status_id", SqlDbType.Int, 4),
                new SqlParameter("@Order_amount", SqlDbType.Float, 8),
                new SqlParameter("@create_id", SqlDbType.Int, 4),
                new SqlParameter("@create_date", SqlDbType.DateTime),
                new SqlParameter("@C_dep_id", SqlDbType.Int, 4),
                new SqlParameter("@C_emp_id", SqlDbType.Int, 4),
                new SqlParameter("@F_dep_id", SqlDbType.Int, 4),
                new SqlParameter("@F_emp_id", SqlDbType.Int, 4),
                new SqlParameter("@receive_money", SqlDbType.Float, 8),
                new SqlParameter("@arrears_money", SqlDbType.Float, 8),
                new SqlParameter("@invoice_money", SqlDbType.Float, 8),
                new SqlParameter("@arrears_invoice", SqlDbType.Float, 8),
                new SqlParameter("@isDelete", SqlDbType.Int, 4),
                new SqlParameter("@Delete_time", SqlDbType.DateTime)
            };
            parameters[0].Value = model.Serialnumber;
            parameters[1].Value = model.Customer_id;
            parameters[2].Value = model.Order_date;
            parameters[3].Value = model.pay_type_id;
            parameters[4].Value = model.Order_details;
            parameters[5].Value = model.Order_status_id;
            parameters[6].Value = model.Order_amount;
            parameters[7].Value = model.create_id;
            parameters[8].Value = model.create_date;
            parameters[9].Value = model.C_dep_id;
            parameters[10].Value = model.C_emp_id;
            parameters[11].Value = model.F_dep_id;
            parameters[12].Value = model.F_emp_id;
            parameters[13].Value = model.receive_money;
            parameters[14].Value = model.arrears_money;
            parameters[15].Value = model.invoice_money;
            parameters[16].Value = model.arrears_invoice;
            parameters[17].Value = model.isDelete;
            parameters[18].Value = model.Delete_time;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            return Convert.ToInt32(obj);
        }

        /// <summary>
        ///     更新一条数据
        /// </summary>
        public bool Update(Model.CRM_order model)
        {
            var strSql = new StringBuilder();
            strSql.Append("update CRM_order set ");
            strSql.Append("Customer_id=@Customer_id,");
            strSql.Append("Order_date=@Order_date,");
            strSql.Append("pay_type_id=@pay_type_id,");
            strSql.Append("Order_details=@Order_details,");
            strSql.Append("Order_status_id=@Order_status_id,");
            strSql.Append("Order_amount=@Order_amount,");
            strSql.Append("create_id=@create_id,");
            strSql.Append("create_date=@create_date,");
            strSql.Append("C_dep_id=@C_dep_id,");
            strSql.Append("C_emp_id=@C_emp_id,");
            strSql.Append("F_dep_id=@F_dep_id,");
            strSql.Append("F_emp_id=@F_emp_id,");
            strSql.Append("receive_money=@receive_money,");
            strSql.Append("arrears_money=@arrears_money,");
            strSql.Append("invoice_money=@invoice_money,");
            strSql.Append("arrears_invoice=@arrears_invoice");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters =
            {
                new SqlParameter("@Customer_id", SqlDbType.Int, 4),
                new SqlParameter("@Order_date", SqlDbType.DateTime),
                new SqlParameter("@pay_type_id", SqlDbType.Int, 4),
                new SqlParameter("@Order_details", SqlDbType.VarChar, -1),
                new SqlParameter("@Order_status_id", SqlDbType.Int, 4),
                new SqlParameter("@Order_amount", SqlDbType.Float, 8),
                new SqlParameter("@create_id", SqlDbType.Int, 4),
                new SqlParameter("@create_date", SqlDbType.DateTime),
                new SqlParameter("@C_dep_id", SqlDbType.Int, 4),
                new SqlParameter("@C_emp_id", SqlDbType.Int, 4),
                new SqlParameter("@F_dep_id", SqlDbType.Int, 4),
                new SqlParameter("@F_emp_id", SqlDbType.Int, 4),
                new SqlParameter("@receive_money", SqlDbType.Float, 8),
                new SqlParameter("@arrears_money", SqlDbType.Float, 8),
                new SqlParameter("@invoice_money", SqlDbType.Float, 8),
                new SqlParameter("@arrears_invoice", SqlDbType.Float, 8),
                new SqlParameter("@isDelete", SqlDbType.Int, 4),
                new SqlParameter("@Delete_time", SqlDbType.DateTime),
                new SqlParameter("@id", SqlDbType.Int, 4)
            };

            parameters[0].Value = model.Customer_id;
            parameters[1].Value = model.Order_date;
            parameters[2].Value = model.pay_type_id;
            parameters[3].Value = model.Order_details;
            parameters[4].Value = model.Order_status_id;
            parameters[5].Value = model.Order_amount;
            parameters[6].Value = model.create_id;
            parameters[7].Value = model.create_date;
            parameters[8].Value = model.C_dep_id;
            parameters[9].Value = model.C_emp_id;
            parameters[10].Value = model.F_dep_id;
            parameters[11].Value = model.F_emp_id;
            parameters[12].Value = model.receive_money;
            parameters[13].Value = model.arrears_money;
            parameters[14].Value = model.invoice_money;
            parameters[15].Value = model.arrears_invoice;
            parameters[16].Value = model.isDelete;
            parameters[17].Value = model.Delete_time;
            parameters[18].Value = model.id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     删除一条数据
        /// </summary>
        public bool Delete(int id)
        {
            var strSql = new StringBuilder();
            strSql.Append("delete from CRM_order ");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters =
            {
                new SqlParameter("@id", SqlDbType.Int, 4)
            };
            parameters[0].Value = id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     批量删除数据
        /// </summary>
        public bool DeleteList(string idlist)
        {
            var strSql = new StringBuilder();
            strSql.Append("delete from CRM_order ");
            strSql.Append(" where id in (" + idlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            return false;
        }


        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            var strSql = new StringBuilder();
            strSql.Append(
                "select id,Serialnumber,Customer_id,Order_date,pay_type_id,Order_details,Order_status_id,Order_amount,create_id,create_date,C_dep_id,C_emp_id,F_dep_id,F_emp_id,receive_money,arrears_money,invoice_money,arrears_invoice,isDelete,Delete_time ");
            strSql.Append(" ,( select Customer from CRM_Customer where id=CRM_order.[Customer_id] ) as [Customer_name] ");
            strSql.Append(" ,( select params_name from Param_SysParam where id=CRM_order.[pay_type_id]) as [pay_type] ");
            strSql.Append(
                " ,( select params_name from Param_SysParam where id=CRM_order.[Order_status_id]) as [Order_status] ");
            strSql.Append(" ,( select d_name from hr_department where id=CRM_order.[C_dep_id]) as [C_dep_name] ");
            strSql.Append(" ,( select name from hr_employee where id = CRM_order.[C_emp_id]) as [C_emp_name] ");
            strSql.Append(" ,( select d_name from hr_department where id=CRM_order.[F_dep_id]) as [F_dep_name] ");
            strSql.Append(" ,( select name from hr_employee where id = CRM_order.[F_emp_id]) as [F_emp_name] ");
            strSql.Append(" FROM CRM_order ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        ///     获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            var strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top);
            }
            strSql.Append(
                " id,Serialnumber,Customer_id,Order_date,pay_type_id,Order_details,Order_status_id,Order_amount,create_id,create_date,C_dep_id,C_emp_id,F_dep_id,F_emp_id,receive_money,arrears_money,invoice_money,arrears_invoice,isDelete,Delete_time ");
            strSql.Append(" ,( select Customer from CRM_Customer where id=CRM_order.[Customer_id] ) as [Customer_name] ");
            strSql.Append(" ,( select params_name from Param_SysParam where id=CRM_order.[pay_type_id]) as [pay_type] ");
            strSql.Append(
                " ,( select params_name from Param_SysParam where id=CRM_order.[Order_status_id]) as [Order_status] ");
            strSql.Append(" ,( select d_name from hr_department where id=CRM_order.[C_dep_id]) as [C_dep_name] ");
            strSql.Append(" ,( select name from hr_employee where id = CRM_order.[C_emp_id]) as [C_emp_name] ");
            strSql.Append(" ,( select d_name from hr_department where id=CRM_order.[F_dep_id]) as [F_dep_name] ");
            strSql.Append(" ,( select name from hr_employee where id = CRM_order.[F_emp_id]) as [F_emp_name] ");
            strSql.Append(" FROM CRM_order ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        ///     分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
        {
            var strSql_grid = new StringBuilder();
            var strSql_total = new StringBuilder();
            strSql_total.Append(" SELECT COUNT(id) FROM CRM_order ");
            strSql_grid.Append("SELECT ");
            strSql_grid.Append(
                "      id,Serialnumber,Customer_id,Order_date,pay_type_id,Order_details,Order_status_id,Order_amount,create_id,create_date,C_dep_id,C_emp_id,F_dep_id,F_emp_id,receive_money,arrears_money,invoice_money,arrears_invoice,isDelete,Delete_time ");
            strSql_grid.Append(" ,( select Customer from CRM_Customer where id=w1.[Customer_id] ) as [Customer_name] ");
            strSql_grid.Append(" ,( select params_name from Param_SysParam where id=w1.[pay_type_id]) as [pay_type] ");
            strSql_grid.Append(
                " ,( select params_name from Param_SysParam where id=w1.[Order_status_id]) as [Order_status] ");
            strSql_grid.Append(" ,( select d_name from hr_department where id=w1.[C_dep_id]) as [C_dep_name] ");
            strSql_grid.Append(" ,( select name from hr_employee where id = w1.[C_emp_id]) as [C_emp_name] ");
            strSql_grid.Append(" ,( select d_name from hr_department where id=w1.[F_dep_id]) as [F_dep_name] ");
            strSql_grid.Append(" ,( select name from hr_employee where id = w1.[F_emp_id]) as [F_emp_name] ");
            strSql_grid.Append(
                " FROM ( SELECT id,Serialnumber,Customer_id,Order_date,pay_type_id,Order_details,Order_status_id,Order_amount,create_id,create_date,C_dep_id,C_emp_id,F_dep_id,F_emp_id,receive_money,arrears_money,invoice_money,arrears_invoice,isDelete,Delete_time, ROW_NUMBER() OVER( Order by " +
                filedOrder + " ) AS n from CRM_order");
            if (strWhere.Trim() != "")
            {
                strSql_grid.Append(" WHERE " + strWhere);
                strSql_total.Append(" WHERE " + strWhere);
            }
            strSql_grid.Append("  ) as w1  ");
            strSql_grid.Append("WHERE n BETWEEN " + PageSize*(PageIndex - 1) + " AND " + PageSize*PageIndex);
            strSql_grid.Append(" ORDER BY " + filedOrder);
            Total = DbHelperSQL.Query(strSql_total.ToString()).Tables[0].Rows[0][0].ToString();
            return DbHelperSQL.Query(strSql_grid.ToString());
        }

        #endregion  BasicMethod

        #region  ExtensionMethod

        /// <summary>
        ///     批量
        /// </summary>
        public bool Update_batch(Model.CRM_order model)
        {
            var strSql = new StringBuilder();
            strSql.Append("update CRM_order set ");
            strSql.Append("F_dep_id=@F_dep_id,");
            strSql.Append("F_emp_id=@F_emp_id");
            strSql.Append(" where F_emp_id=@create_id");
            SqlParameter[] parameters =
            {
                new SqlParameter("@F_dep_id", SqlDbType.Int, 4),
                new SqlParameter("@F_emp_id", SqlDbType.Int, 4),
                new SqlParameter("@create_id", SqlDbType.Int, 4)
            };

            parameters[0].Value = model.F_dep_id;
            parameters[1].Value = model.F_emp_id;
            parameters[2].Value = model.create_id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     更新发票
        /// </summary>
        public bool UpdateInvoice(int orderid)
        {
            var strSql1 = new StringBuilder();
            strSql1.Append(" /*更新发票*/ ");
            strSql1.Append(" UPDATE CRM_order SET ");
            strSql1.Append("     invoice_money=(SELECT SUM(ISNULL(invoice_amount,0)) AS Expr1 FROM CRM_invoice WHERE order_id=" +
                orderid + ")  ");
            strSql1.Append(" WHERE (id=" + orderid + ") ");

            var strSql2 = new StringBuilder();
            strSql2.Append(" /*更新发票*/ ");
            strSql2.Append(" UPDATE CRM_order SET ");
            strSql2.Append("     arrears_invoice= ISNULL(Order_amount,0) - ISNULL(invoice_money,0)  ");
            strSql2.Append(" WHERE (id=" + orderid + ") ");

            int rows1 = DbHelperSQL.ExecuteSql(strSql1.ToString());
            int rows2 = DbHelperSQL.ExecuteSql(strSql2.ToString());

            if (rows1 > 0 && rows2 > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     更新发票
        /// </summary>
        public bool UpdateReceive(int orderid)
        {
            var strSql1 = new StringBuilder();
            strSql1.Append(" /*更新收款*/ ");
            strSql1.Append(" UPDATE CRM_order SET ");
            strSql1.Append("     receive_money=(SELECT SUM(ISNULL(Receive_amount,0)) AS Expr1 FROM CRM_receive WHERE  order_id=" +orderid + ")  ");
            strSql1.Append(" WHERE (id=" + orderid + ") ");

            var strSql2 = new StringBuilder();
            strSql2.Append(" /*更新收款*/ ");
            strSql2.Append(" UPDATE CRM_order SET ");
            strSql2.Append("     arrears_money= ISNULL(Order_amount,0) - ISNULL(receive_money,0)  ");
            strSql2.Append(" WHERE (id=" + orderid + ") ");

            int rows1 = DbHelperSQL.ExecuteSql(strSql1.ToString());
            int rows2 = DbHelperSQL.ExecuteSql(strSql2.ToString());

            if (rows1 > 0 && rows2 > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///  更新收款和发票客户ID
        /// </summary>
        /// <param name="orderid"></param>
        /// <returns></returns>
        public bool UpdateCustomerID(int orderid,int CustomerID)
        {
            //receive
            var strSql1 = new StringBuilder();
            strSql1.Append(" UPDATE CRM_receive SET ");
            strSql1.Append("     Customer_id=" + CustomerID );
            strSql1.Append(" WHERE order_id=" + orderid );

            //invoice
            var strSql2 = new StringBuilder();
            strSql2.Append(" UPDATE CRM_invoice SET ");
            strSql2.Append("     Customer_id=" + CustomerID);
            strSql2.Append(" WHERE order_id=" + orderid);

            int rows1 = DbHelperSQL.ExecuteSql(strSql1.ToString());
            int rows2 = DbHelperSQL.ExecuteSql(strSql2.ToString());

            if (rows1 > 0 && rows2 > 0)
            {
                return true;
            }
            return false;
        }

        #endregion  ExtensionMethod
    }
}