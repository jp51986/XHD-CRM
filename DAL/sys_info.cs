/*
* sys_info.cs
*
* 功 能： N/A
* 类 名： sys_info
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:53:47    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System.Data;
using System.Data.SqlClient;
using System.Text;
using XHD.DBUtility;

//Please add references

namespace XHD.DAL
{
    /// <summary>
    ///     数据访问类:sys_info
    /// </summary>
    public class sys_info
    {
        #region  Method

        /// <summary>
        ///     更新一条数据
        /// </summary>
        public bool Update(Model.sys_info model)
        {
            var strSql = new StringBuilder();
            strSql.Append("update sys_info set ");
            strSql.Append("sys_value=@sys_value");
            strSql.Append(" where sys_key=@sys_key");
            SqlParameter[] parameters =
            {
                new SqlParameter("@sys_key", SqlDbType.VarChar, 50),
                new SqlParameter("@sys_value", SqlDbType.VarChar)
            };
            parameters[0].Value = model.sys_key;
            parameters[1].Value = model.sys_value;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            var strSql = new StringBuilder();
            strSql.Append("select id,sys_key,sys_value ");
            strSql.Append(" FROM sys_info ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        #endregion  Method
    }
}