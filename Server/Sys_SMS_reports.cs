﻿/*
* Sys_SMS_reports.cs
*
* 功 能： N/A
* 类 名： Sys_SMS_reports
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Data;
using System.Web;
using XHD.Common;
using XHD.Controller;

namespace XHD.Server
{
    public class Sys_SMS_reports
    {
        public static BLL.Sys_SMS_reports report = new BLL.Sys_SMS_reports();
        public static Model.Sys_SMS_reports model = new Model.Sys_SMS_reports();

        public HttpContext Context;
        public int emp_id;
        public string emp_name;
        public Model.hr_employee employee;
        public HttpRequest request;
        public string uid;


        public Sys_SMS_reports()
        {
        }

        public Sys_SMS_reports(HttpContext context)
        {
            Context = context;
            request = context.Request;

            var userinfo = new User_info();
            employee = userinfo.GetCurrentEmpInfo(context);

            emp_id = employee.ID;
            emp_name = PageValidate.InputText(employee.name, 50);
            uid = PageValidate.InputText(employee.uid, 50);
        }

        public string getStatus()
        {
            int PageIndex = int.Parse(request["page"] == null ? "1" : request["page"]);
            int PageSize = int.Parse(request["pagesize"] == null ? "30" : request["pagesize"]);
            string sortname = request["sortname"];
            string sortorder = request["sortorder"];

            if (string.IsNullOrEmpty(sortname))
                sortname = " id ";
            if (string.IsNullOrEmpty(sortorder))
                sortorder = " desc";

            string sorttext = " " + sortname + " " + sortorder;

            string Total;
            string serchtxt = "1=1";
            string id = PageValidate.InputText(request["id"], 50);
            if (PageValidate.IsNumber(id))
            {
                serchtxt += string.Format(" and seqID={0}", id);

                DataSet ds = report.GetList(PageSize, PageIndex,serchtxt , sorttext, out Total);

                string dt = GetGridJSON.DataTableToJSON1(ds.Tables[0], Total);

                return dt;
            }
            else
            {
                return "{ \"Rows\":[],\"Total\":0}";
            }
        }

    }
}
