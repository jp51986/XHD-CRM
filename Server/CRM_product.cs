﻿/*
* CRM_product.cs
*
* 功 能： N/A
* 类 名： CRM_product
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System.Data;
using System.Web;
using XHD.Common;
using XHD.Controller;

namespace XHD.Server
{
    public class CRM_product
    {
        public static BLL.CRM_product product = new BLL.CRM_product();
        public static Model.CRM_product model = new Model.CRM_product();

        public HttpContext Context;
        public int emp_id;
        public string emp_name;
        public Model.hr_employee employee;
        public HttpRequest request;
        public string uid;


        public CRM_product()
        {
        }

        public CRM_product(HttpContext context)
        {
            Context = context;
            request = context.Request;

            var userinfo = new User_info();
            employee = userinfo.GetCurrentEmpInfo(context);

            emp_id = employee.ID;
            emp_name = PageValidate.InputText(employee.name, 50);
            uid = PageValidate.InputText(employee.uid, 50);
        }

        public void save()
        {
            model.category_id = int.Parse(request["T_product_category_val"]);
            model.product_name = PageValidate.InputText(request["T_product_name"], 255);
            model.specifications = PageValidate.InputText(request["T_specifications"], 255);
            model.unit = PageValidate.InputText(request["T_product_unit"], 255);
            model.remarks = PageValidate.InputText(request["T_remarks"], 255);
            model.price = decimal.Parse(request["T_price"]);

            string pid = PageValidate.InputText(request["pid"], 50);
            if (PageValidate.IsNumber(pid))
            {
                model.product_id = int.Parse(pid);
                DataSet ds = product.GetList(" product_id=" + int.Parse(pid));
                DataRow dr = ds.Tables[0].Rows[0];
                product.Update(model);

                //日志
                var log = new sys_log();

                int UserID = emp_id;
                string UserName = emp_name;
                string IPStreet = request.UserHostAddress;
                string EventTitle = model.product_name;
                string EventType = "产品修改";
                int EventID = model.product_id;

                string Log_Content = null;

                if (dr["category_name"].ToString() != request["T_product_category"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "产品类别", dr["category_name"],
                        request["T_product_category"]);

                if (dr["product_name"].ToString() != request["T_product_name"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "产品名字", dr["product_name"],
                        request["T_product_name"]);

                if (dr["specifications"].ToString() != request["T_specifications"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "产品规格", dr["specifications"],
                        request["T_specifications"]);

                if (dr["unit"].ToString() != request["T_product_unit"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "单位", dr["unit"], request["T_product_unit"]);

                if (dr["remarks"].ToString() != request["T_remarks"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "备注", dr["remarks"], request["T_remarks"]);

                if (dr["price"].ToString() != request["T_price"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "价格", dr["price"], request["T_price"]);

                if (!string.IsNullOrEmpty(Log_Content))
                    log.Add_log(UserID, UserName, IPStreet, EventTitle, EventType, EventID, Log_Content);
            }
            else
            {
                model.isDelete = 0;
                product.Add(model);
            }
        }

        public string grid()
        {
            int PageIndex = int.Parse(request["page"] == null ? "1" : request["page"]);
            int PageSize = int.Parse(request["pagesize"] == null ? "30" : request["pagesize"]);
            string sortname = request["sortname"];
            string sortorder = request["sortorder"];

            if (string.IsNullOrEmpty(sortname))
                sortname = " category_id";
            if (string.IsNullOrEmpty(sortorder))
                sortorder = "desc";

            string sorttext = " " + sortname + " " + sortorder;

            string Total;
            string serchtxt = "1=1";
            if (!string.IsNullOrEmpty(request["categoryid"]))
                serchtxt += string.Format(" and category_id={0}", int.Parse(request["categoryid"]));

            if (!string.IsNullOrEmpty(request["stext"]))
                serchtxt += " and product_name like N'%" + PageValidate.InputText(request["stext"], 255) + "%'";

            //权限
            DataSet ds = product.GetList(PageSize, PageIndex, serchtxt, sorttext, out Total);

            string dt = GetGridJSON.DataTableToJSON1(ds.Tables[0], Total);
            return (dt);
        }

        public string form(string id)
        {
            DataSet ds;
            string dt;
            if (PageValidate.IsNumber(id))
            {
                ds = product.GetList(" product_id=" + id);
                dt = DataToJson.DataToJSON(ds);
            }
            else
            {
                dt = "{}";
            }

            return (dt);
        }

        //del
        public string del(int id)
        {
            DataSet ds = product.GetList(" product_id=" + id);

            var ccod = new BLL.CRM_order_details();
            if (ccod.GetList("product_id=" + id).Tables[0].Rows.Count > 0)
            {
                //order
                return ("false:order");
            }
            bool isdel = product.Delete(id);
            if (isdel)
            {
                //日志
                string EventType = "产品删除";

                int UserID = emp_id;
                string UserName = emp_name;
                string IPStreet = request.UserHostAddress;
                int EventID = id;
                string EventTitle = ds.Tables[0].Rows[0]["product_name"].ToString();

                var log = new sys_log();

                log.Add_log(UserID, UserName, IPStreet, EventTitle, EventType, EventID, null);

                return ("true");
            }
            return ("false");
        }
    }
}