﻿/*
* tool_batch.cs
*
* 功 能： N/A
* 类 名： tool_batch
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Data;
using System.Web;
using XHD.Common;
using XHD.Controller;

namespace XHD.Server
{
    public class tool_batch
    {
        public static BLL.tool_batch batch = new BLL.tool_batch();
        public static Model.tool_batch model = new Model.tool_batch();

        public HttpContext Context;
        public int emp_id;
        public string emp_name;
        public Model.hr_employee employee;
        public HttpRequest request;
        public string uid;


        public tool_batch()
        {
        }

        public tool_batch(HttpContext context)
        {
            Context = context;
            request = context.Request;

            var userinfo = new User_info();
            employee = userinfo.GetCurrentEmpInfo(context);

            emp_id = employee.ID;
            emp_name = PageValidate.InputText(employee.name, 50);
            uid = PageValidate.InputText(employee.uid, 50);
        }

        public void save()
        {
            model.batch_type = PageValidate.InputText(request["type"], 50);
            model.b_count = 0;

            model.o_dep_id = int.Parse(request["T_dep1_val"]);
            model.o_emp_id = int.Parse(request["T_employee1_val"]);

            model.c_dep_id = int.Parse(request["T_dep2_val"]);
            model.c_emp_id = int.Parse(request["T_employee2_val"]);

            model.create_id = emp_id;
            model.create_date = DateTime.Now;

            switch (model.batch_type)
            {
                case "customer":

                    string serchtxt = " isDelete=0 ";

                    if (!string.IsNullOrEmpty(request["T_employee1_val"]))
                        serchtxt += string.Format(" and Employee_id={0}",
                            PageValidate.InputText(request["T_employee1_val"], 50));

                    if (!string.IsNullOrEmpty(request["T_customertype"]))
                        serchtxt += " and CustomerType_id = " + int.Parse(request["T_customertype_val"]);

                    if (!string.IsNullOrEmpty(request["T_customerlevel"]))
                        serchtxt += " and CustomerLevel_id = " + int.Parse(request["T_customerlevel_val"]);

                    if (!string.IsNullOrEmpty(request["T_CustomerSource"]))
                        serchtxt += " and CustomerSource_id = " + int.Parse(request["T_CustomerSource_val"]);

                    if (!string.IsNullOrEmpty(request["startdate"]))
                        serchtxt += " and Create_date >= '" + PageValidate.InputText(request["startdate"], 255) + "'";

                    if (!string.IsNullOrEmpty(request["enddate"]))
                    {
                        DateTime enddate = DateTime.Parse(request["enddate"]).AddHours(23).AddMinutes(59).AddSeconds(59);
                        serchtxt += " and Create_date <= '" + enddate + "'";
                    }

                    if (!string.IsNullOrEmpty(request["startfollow"]))
                        serchtxt += " and lastfollow >= '" + PageValidate.InputText(request["startfollow"], 255) + "'";

                    if (!string.IsNullOrEmpty(request["endfollow"]))
                    {
                        DateTime enddate =
                            DateTime.Parse(request["endfollow"]).AddHours(23).AddMinutes(59).AddSeconds(59);
                        serchtxt += " and lastfollow <= '" + enddate + "'";
                    }

                    var customer = new BLL.CRM_Customer();
                    var model_cus = new Model.CRM_Customer();

                    model.b_count =
                        customer.GetList(string.Format("Employee_id={0} and {1}", model.o_emp_id, serchtxt)).Tables[0]
                            .Rows.Count;

                    model_cus.Department_id = model.c_dep_id;
                    model_cus.Employee_id = model.c_emp_id;
                    model_cus.Create_id = model.o_emp_id;

                    customer.Update_batch(model_cus, serchtxt);
                    break;

                case "order":
                    var order = new BLL.CRM_order();
                    var model_order = new Model.CRM_order();

                    model.b_count = order.GetList(string.Format("F_emp_id={0}", model.o_emp_id)).Tables[0].Rows.Count;

                    model_order.F_dep_id = model.c_dep_id;
                    model_order.F_emp_id = model.c_emp_id;
                    model_order.create_id = model.o_emp_id;

                    order.Update_batch(model_order);
                    break;
            }

            batch.Add(model);
        }

        public string grid()
        {
            int PageIndex = int.Parse(request["page"] == null ? "1" : request["page"]);
            int PageSize = int.Parse(request["pagesize"] == null ? "30" : request["pagesize"]);
            string sortname = request["sortname"];
            string sortorder = request["sortorder"];

            if (string.IsNullOrEmpty(sortname))
                sortname = " id ";
            if (string.IsNullOrEmpty(sortorder))
                sortorder = " desc";

            string sorttext = " " + sortname + " " + sortorder;

            string Total;
            string serchtxt = "1=1";

            //context.Response.Write(serchtxt);

            DataSet ds = batch.GetList(PageSize, PageIndex, serchtxt, sorttext, out Total);

            string dt = GetGridJSON.DataTableToJSON1(ds.Tables[0], Total);
            return dt;
        }
    }
}