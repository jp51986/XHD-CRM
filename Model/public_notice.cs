/*
* public_notice.cs
*
* 功 能： N/A
* 类 名： public_notice
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;

namespace XHD.Model
{
    /// <summary>
    ///     public_notice:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public class public_notice
    {
        #region Model

        private int? _create_id;
        private string _create_name;
        private int? _dep_id;
        private string _dep_name;
        private int _id;
        private string _notice_content;
        private DateTime? _notice_time;
        private string _notice_title;

        /// <summary>
        /// </summary>
        public int id
        {
            set { _id = value; }
            get { return _id; }
        }

        /// <summary>
        /// </summary>
        public string notice_title
        {
            set { _notice_title = value; }
            get { return _notice_title; }
        }

        /// <summary>
        /// </summary>
        public string notice_content
        {
            set { _notice_content = value; }
            get { return _notice_content; }
        }

        /// <summary>
        /// </summary>
        public int? create_id
        {
            set { _create_id = value; }
            get { return _create_id; }
        }

        /// <summary>
        /// </summary>
        public string create_name
        {
            set { _create_name = value; }
            get { return _create_name; }
        }

        /// <summary>
        /// </summary>
        public int? dep_id
        {
            set { _dep_id = value; }
            get { return _dep_id; }
        }

        /// <summary>
        /// </summary>
        public string dep_name
        {
            set { _dep_name = value; }
            get { return _dep_name; }
        }

        /// <summary>
        /// </summary>
        public DateTime? notice_time
        {
            set { _notice_time = value; }
            get { return _notice_time; }
        }

        #endregion Model
    }
}