﻿/*
* CRM_product.cs
*
* 功 能： N/A
* 类 名： CRM_product
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-24 10:03:12    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;

namespace XHD.Model
{
    /// <summary>
    ///     CRM_product:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public class CRM_product
    {
        #region Model

        private int? _category_id;
        private DateTime? _delete_time;
        private int? _isdelete;
        private decimal? _price;
        private int _product_id;
        private string _product_name;
        private string _remarks;
        private string _specifications;
        private string _status;
        private string _unit;

        /// <summary>
        /// </summary>
        public int product_id
        {
            set { _product_id = value; }
            get { return _product_id; }
        }

        /// <summary>
        /// </summary>
        public string product_name
        {
            set { _product_name = value; }
            get { return _product_name; }
        }

        /// <summary>
        /// </summary>
        public int? category_id
        {
            set { _category_id = value; }
            get { return _category_id; }
        }

        /// <summary>
        /// </summary>
        public string specifications
        {
            set { _specifications = value; }
            get { return _specifications; }
        }

        /// <summary>
        /// </summary>
        public string status
        {
            set { _status = value; }
            get { return _status; }
        }

        /// <summary>
        /// </summary>
        public string unit
        {
            set { _unit = value; }
            get { return _unit; }
        }

        /// <summary>
        /// </summary>
        public string remarks
        {
            set { _remarks = value; }
            get { return _remarks; }
        }

        /// <summary>
        /// </summary>
        public decimal? price
        {
            set { _price = value; }
            get { return _price; }
        }

        /// <summary>
        /// </summary>
        public int? isDelete
        {
            set { _isdelete = value; }
            get { return _isdelete; }
        }

        /// <summary>
        /// </summary>
        public DateTime? Delete_time
        {
            set { _delete_time = value; }
            get { return _delete_time; }
        }

        #endregion Model
    }
}